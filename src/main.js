import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import vuetify from './plugins/vuetify';
import axios from 'axios';

Vue.config.productionTip = false;

axios.defaults.baseURL = "http://www.boredapi.com/api"
axios.defaults.headers.common["Content-Type"] = "application/json";

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
