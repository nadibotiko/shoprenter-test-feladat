import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    activeTab: JSON.parse(localStorage.getItem('activeTab')) || null,
    activities: JSON.parse(localStorage.getItem('activities')) || [],
  },
  getters: {
    activities(state) {
      return state.activities;
    },
    activeTab(state) {
      return state.activeTab;
    },
  },
  mutations: {
    setActiveTab(state, payload) {
      state.activeTab = payload;
      localStorage.setItem('activeTab', payload);
    },
    addActivity(state, payload) {
      let activities = JSON.parse(localStorage.getItem('activities'));
      if (activities) {
        state.activities = activities;
      }
      state.activities.push(payload);
      localStorage.setItem('activities', JSON.stringify(state.activities));
    },
    updateActivities(state, payload) {
      let activities = JSON.parse(localStorage.getItem('activities'));
      state.activities = activities;
      state.activities = payload;
      localStorage.setItem('activities', JSON.stringify(state.activities));
    },
  },
  actions: {
    saveActiveTab({ commit }, payload) {
      commit('setActiveTab', payload);
    },
    saveActivities({ commit }, payload) {
      commit('addActivity', payload);
    },
    updateActivities({ commit }, payload) {
      commit('updateActivities', payload);
    },
  },
  modules: {},
});


